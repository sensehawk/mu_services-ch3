## Microservices 
### CH-3
+++
@ul
- Service discovery
- Configurations
- Routing
- CI/CD
@ulend
---
## CONSUL
---?color=linear-gradient(60deg, #CA2171, 55%, white 35%)
@snap[west span-50]
@ul[spaced text-white]
- Service Discovery
- Health checks
- Key/Value store
- LAN/WAN datacenter deployments
- Dashboard
@ulend
@snapend

@snap[east]
@img[Consul](assets/img/consul.svg)
@snapend
---?image=assets/img/consul-gossip.png&size=50%
---
@snap[north]
Getting started
@snapend
```bash
# run agent in developer mode, no data is persisted.
$ consul agent -dev

# bootstrap is requied if the agent is the only server in the cluster.
# requires binding to specific addresses
$ consul agent -bootstrap -server -bind <address> -client <address>

# start the client
$ consul agent -join <server-address:{port}>

# list the members
$ consul memebers

# list services
$ consul catalog services
```
---
## Server configuration
+++?code=code/consul-server/agent.json&lang=json&title=~/consul.d/agent.json
@snap[south span-100]
@[4](to persiste state of the cluster)
@[8](for cluster communications [gossip])
@[9](for client access [http/s, dns, gRPC])
@[10](encrypt gossip)
@snapend

+++?code=code/consul-server/acl.json&lang=json&title=~/consul.d/acl.json
+++
@snap[north]
Create certificates
@snapend

```bash
# Create a CA
$ consul tls ca create

# Create a server certificate
$ consul tls cert create -server -dc <datacenter> 
                         -additional-dnsname=<domain>
                         -additional-ipaddress=<address>

# Create a client certificate
$ consul tls cert create -client -dc <datacenter>
```

+++?code=code/consul-server/tls.json&lang=json&title=~/consul.d/tls.json
@[2](to allow traffik to dashboard [careful])
@[3-4](verify communications over tls incoming/outgoing)
@[5](very important to mitigate compromised client restarting as server)
@[7-9](absolute paths to certificates)
@[11-14](remove http and enable https)
+++
@snap[north]
use public ip to advertise.
@snapend
```bash
$ dig +short myip.opendns.com @resolver1.opendns.com | 
  xargs -I{} sudo consul agent -config-dir ~/consul.d/ -advertise {}
```
---
## Client configuration
+++?code=code/consul-client/agent.json&lang=json&title=~/consul-client.d/agent.json
+++?code=code/consul-client/acl.json&lang=json&title=~/consul-client.d/acl.json
+++
+++?code=code/consul-client/tls.json&lang=json&title=~/consul-client.d/tls.json

---
## CLI configuration
+++
@snap[north]
Append to ~/.bashrc
@snapend
```bash
export CONSUL_CACERT=consul-agent-ca.pem
export CONSUL_CLIENT_CERT=sensehawk-primary-cli-consul-0.pem
export CONSUL_CLIENT_KEY=sensehawk-primary-cli-consul-0-key.pem

export CONSUL_HTTP_ADDR=https://192.168.1.200:8501
export CONSUL_HTTP_TOKEN=Joker@123

```
---
## Simple HTTP Service 
+++?code=code/web-client/app/api.py&lang=python&title=~/web-client/app/api.py
+++?code=code/web-client/app/start.sh&lang=bash&title=~/web-client/app/start.sh
+++?code=code/web-client/Dockerfile&title=~/web-client/Dockerfile

---
## Service registration
+++?code=code/consul-client/web.json&lang=json&title=~/consul-client.d/web.json
@[5-8](tags can be used by other applications)
@[9-16](define atleast one http health check)
---
## Consul KV
---
## Consul ACL
+++
@snap[north]
Create a policy
@snapend
@snap[middle]
@img[Add Policy](assets/img/consul-policy-1.png)
@snapend
+++
@snap[north]
Create a token
@snapend
@img[Add Token](assets/img/consul-token-1.png)
---
## Consul Template
+++?code=code/consul-template/consul-template-config.json&lang=json&title=~/consul-template-config.json
+++?code=code/consul-template/consul-template-config-1.json&lang=json&title=~/consul-template-config.json
@[13-22](yes, it takes multiple templates!)

---
## Traefik
---?color=linear-gradient(60deg, #47BCD3, 55%, white 35%)
@snap[west span-50]
@ul[spaced text-white]
- Reverse proxy
- Load balancer (wrr/drr)
- Rich integrations (docker, consul, ecs, etc)
- Dynamic configurations (consul kv)
@ulend
@snapend

@snap[east span-45]
@img[Consul](assets/img/traefik.logo.png)
@snapend
---
@img[Traefik Architecture](assets/img/traefik-arch.png)
+++
@img[Traefik Architecture](assets/img/traefik-arch-2.png)
---?code=code/traefik/traefik-1.toml&lang=toml&title=~/traefik.toml
---
## Consul + Traefik
+++?code=code/consul-client/web-1.json&lang=json&title=~/consul-client.d/web.json
@[8-9](just that is enough)
+++?code=code/consul-client/web-2.json&lang=json&title=~/consul-client.d/web.json
@[9-10](multiple routing patterns)
---
## Built-in ACME support
+++?code=code/traefik/traefik.toml&lang=toml&title=~/traefik.toml
@[4-11](define entrypoints, redirect http->https)
@[23-28](define acme options for acquiring/renewing certificates)
@[28](comment this line in production)
@[30-33](provide challenge details, delay should not be too less)
@[35-37](for wildcard certificates)
---
## |~Q|