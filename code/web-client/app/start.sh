# /bin/bash
nohup consul agent -config-dir /consul.d/ -node $AGENT_NAME >consul.log &
gunicorn api:api --bind 0.0.0.0:80
