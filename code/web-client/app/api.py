import falcon
import os


def sink(req, res):
    res.media = {"message": "Hello from {}".format(
        os.getenv("AGENT_NAME", "N/A"))}


api = falcon.API()
api.add_sink(sink, prefix="/")
